// Thomas Nagy 2007-2023 GPLV3

#include "browser.h"
#include <QtDebug>
#include <KLocalizedString>
#include <QMenu>
#include <QContextMenuEvent>

browser::browser(QWidget *w) : QWebEngineView(w)
{
	setContextMenuPolicy(Qt::DefaultContextMenu);
	m_oMenu = NULL;
}

void browser::contextMenuEvent(QContextMenuEvent * event)
{
	if (!m_oMenu)
	{
		m_oMenu = new QMenu(this);
        m_oMenu->addAction(pageAction(QWebEnginePage::Reload));

	}
	m_oMenu->popup(event->globalPos());

}

